const assert = require('assert');
const JamesAppTemplate = require('../index');

describe('0 Models registered', function () {
  let app = new JamesAppTemplate();
  it('app.modules', () => {
    assert.equal(Object.getPrototypeOf(app.modules), Map.prototype, 'app.modules is not a Map');
    assert.equal(app.modules.size, 0, 'app.modules.size is not 0');
  });

  it('getModulesSortedByDependency', () => {
    let sortedModules = app.getModulesSortedByDependency();
    assert.equal(Array.isArray(sortedModules), true, 'returned value is not an array');
    assert.equal(sortedModules.length, 0, 'array is not empty');
  });
});
