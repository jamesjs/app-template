const assert = require('assert');
const JamesAppTemplate = require('../index');

describe('2 module registered calling "registerModules" with dependency loop', () => {
  let app = new JamesAppTemplate();

  let module1 = {
    init: (caller) => {
    },
    package: {
      name: 'test-module',
      version: '1.0.0',
      dependencies: {
        'test-module2': '2.0.0'
      }
    }
  };
  let module2 = {
    init: (caller) => {
    },
    package: {
      name: 'test-module2',
      version: '2.0.0',
      dependencies: {
        'test-module': '1.0.0'
      }
    }
  };
  it('avoid endless loop at dependencies', async() => {
    let error = null;
    try {
      await app.registerModules([module1, module2]);
      app.getModulesSortedByDependency();
    } catch (e) {
      error = e
    }
    assert.notEqual(error, null, 'error was not thrown');
    assert.equal(Object.getPrototypeOf(error), Error.prototype, 'Error is not of type Error');
    assert.equal(error.message, 'Endless loop at module dependencies', 'Wrong error message');
  });
});