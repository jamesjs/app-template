const assert = require('assert');
const JamesAppTemplate = require('../index');

describe('1 module registered calling "registerModule"', () => {
  let initCalled = false;
  let initCaller = null;
  let moduleRegisteredCalled = false;
  let moduleInfo = null;
  let app = new JamesAppTemplate();
  let module = {
    init: (caller) => {
      initCalled = true;
      initCaller = caller;
      caller.on('moduleRegistered', (info) => {
        moduleRegisteredCalled = true;
        moduleInfo = info;
      })
    },
    package: {
      name: 'test-module',
      version: '1.0.0',
      dependencies: {}
    }
  };
  it('app.modules', async() => {
    await app.registerModule(module);
    assert.equal(Object.getPrototypeOf(app.modules), Map.prototype, 'app.modules is not a Map');
    assert.equal(app.modules.size, 1, 'app.modules.size is not 1');
  });
  it('module init', () => {
    assert.equal(initCalled, true, 'init was not called');
    assert.equal(initCaller.constructor.name, 'JamesAppTemplate', 'caller of init is not "JamesAppTemplate"');
  });
  it('module event "moduleRegistered"', () => {
    assert.equal(moduleRegisteredCalled, true, 'event was not fired');
    assert.equal(Object.getPrototypeOf(moduleInfo), Object.prototype, 'moduleInfo is not of type Object');
    assert.equal(moduleInfo.name, module.package.name, 'wrong module name was provided in event');
    assert.equal(moduleInfo.version, module.package.version, 'wrong module version was provided in event');
  });
  it('app.getModulesSortedByDependency', () => {
    let sortedModules = app.getModulesSortedByDependency();
    assert.equal(Array.isArray(sortedModules), true);
    assert.equal(sortedModules.length, 1);
    assert.equal(sortedModules[0], 'test-module');
  });
});

describe('1 module registered calling "registerModules"', () => {
  let initCalled = false;
  let initCaller = null;
  let moduleRegisteredCalled = false;
  let moduleInfo = null;
  let app = new JamesAppTemplate();
  let module = {
    init: (caller) => {
      initCalled = true;
      initCaller = caller;
      caller.on('moduleRegistered', (info) => {
        moduleRegisteredCalled = true;
        moduleInfo = info;
      })
    },
    package: {
      name: 'test-module',
      version: '1.0.0',
      dependencies: {}
    }
  };
  it('app.modules', async() => {
    await app.registerModules([module]);
    assert.equal(Object.getPrototypeOf(app.modules), Map.prototype, 'app.modules is not a Map');
    assert.equal(app.modules.size, 1, 'app.modules.size is not 1');
  });
  it('module init', () => {
    assert.equal(initCalled, true, 'init was not called');
    assert.equal(initCaller.constructor.name, 'JamesAppTemplate', 'caller of init is not "JamesAppTemplate"');
  });
  it('module event "moduleRegistered"', () => {
    assert.equal(moduleRegisteredCalled, true, 'event was not fired');
    assert.equal(Object.getPrototypeOf(moduleInfo), Object.prototype, 'moduleInfo is not of type Object');
    assert.equal(moduleInfo.name, module.package.name, 'wrong module name was provided in event');
    assert.equal(moduleInfo.version, module.package.version, 'wrong module version was provided in event');
  });
  it('app.getModulesSortedByDependency', () => {
    let sortedModules = app.getModulesSortedByDependency();
    assert.equal(Array.isArray(sortedModules), true);
    assert.equal(sortedModules.length, 1);
    assert.equal(sortedModules[0], 'test-module');
  });
});