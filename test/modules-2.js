const assert = require('assert');
const JamesAppTemplate = require('../index');

describe('2 module registered calling "registerModules" in correct dependecy order', () => {
  let init1Called = false;
  let init1Caller = null;
  let module1RegisteredCalledTimes = 0;
  let module1Info = [];

  let init2Called = false;
  let init2Caller = null;
  let module2RegisteredCalledTimes = 0;
  let module2Info = [];

  let app = new JamesAppTemplate();

  let module1 = {
    init: (caller) => {
      init1Called = true;
      init1Caller = caller;
      caller.on('moduleRegistered', (info) => {
        module1RegisteredCalledTimes++;
        module1Info.push(info);
      })
    },
    package: {
      name: 'test-module',
      version: '1.0.0',
      dependencies: {}
    }
  };
  let module2 = {
    init: (caller) => {
      init2Called = true;
      init2Caller = caller;
      caller.on('moduleRegistered', (info) => {
        module2RegisteredCalledTimes++;
        module2Info.push(info);
      })
    },
    package: {
      name: 'test-module2',
      version: '2.0.0',
      dependencies: {
        'test-module': '1.0.0'
      }
    }
  };
  it('app.modules', async() => {
    await app.registerModules([module1, module2]);
    assert.equal(Object.getPrototypeOf(app.modules), Map.prototype, 'app.modules is not a Map');
    assert.equal(app.modules.size, 2, 'app.modules.size is not 2');
  });
  it('module 1 init', () => {
    assert.equal(init1Called, true, 'init was not called');
    assert.equal(init1Caller.constructor.name, 'JamesAppTemplate', 'caller of init is not "JamesAppTemplate"');
  });
  it('module 1 event "moduleRegistered"', () => {
    assert.equal(module1RegisteredCalledTimes, 2, 'event was not fired 2 times');

    assert.equal(Object.getPrototypeOf(module1Info[0]), Object.prototype, 'first event: moduleInfo is not of type Object');
    assert.equal(module1Info[0].name, module1.package.name, 'first event: wrong module name was provided in event');
    assert.equal(module1Info[0].version, module1.package.version, 'first event: wrong module version was provided in event');

    assert.equal(Object.getPrototypeOf(module1Info[1]), Object.prototype, 'second event: moduleInfo is not of type Object');
    assert.equal(module1Info[1].name, module2.package.name, 'second event: wrong module name was provided in event');
    assert.equal(module1Info[1].version, module2.package.version, 'second event: wrong module version was provided in event');
  });
  it('module 2 init', () => {
    assert.equal(init2Called, true, 'init was not called');
    assert.equal(init2Caller.constructor.name, 'JamesAppTemplate', 'caller of init is not "JamesAppTemplate"');
  });
  it('module 2 event "moduleRegistered"', () => {
    assert.equal(module2RegisteredCalledTimes, true, 'event was not fired');
    assert.equal(Object.getPrototypeOf(module2Info[0]), Object.prototype, 'moduleInfo is not of type Object');
    assert.equal(module2Info[0].name, module2.package.name, 'wrong module name was provided in event');
    assert.equal(module2Info[0].version, module2.package.version, 'wrong module version was provided in event');
  });
  it('app.getModulesSortedByDependency', () => {
    let sortedModules = app.getModulesSortedByDependency();
    assert.equal(Array.isArray(sortedModules), true);
    assert.equal(sortedModules.length, 2);
    assert.equal(sortedModules[0], 'test-module');
    assert.equal(sortedModules[1], 'test-module2');
  });
});

describe('2 module registered calling "registerModules" in wrong dependecy order', () => {
  let app = new JamesAppTemplate();

  let module1 = {
    init: () => {},
    package: {
      name: 'test-module',
      version: '1.0.0',
      dependencies: {}
    }
  };
  let module2 = {
    init: () => {},
    package: {
      name: 'test-module2',
      version: '2.0.0',
      dependencies: {
        'test-module': '1.0.0'
      }
    }
  };
  it('app.getModulesSortedByDependency', async () => {
    await app.registerModules([module2, module1]);
    let sortedModules = app.getModulesSortedByDependency();
    assert.equal(Array.isArray(sortedModules), true);
    assert.equal(sortedModules.length, 2);
    assert.equal(sortedModules[0], 'test-module');
    assert.equal(sortedModules[1], 'test-module2');
  });
});

describe('2 module registered calling "registerModules" with optional dependency', () => {
  let app = new JamesAppTemplate();

  let module1 = {
    init: () => {},
    package: {
      name: 'test-module',
      version: '1.0.0',
      optionalDependencies: {
        'test-module2': '2.0.0'
      }
    }
  };
  let module2 = {
    init: () => {},
    package: {
      name: 'test-module2',
      version: '2.0.0'
    }
  };
  it('app.getModulesSortedByDependency', async () => {
    await app.registerModules([module2, module1]);
    let sortedModules = app.getModulesSortedByDependency();
    assert.equal(Array.isArray(sortedModules), true);
    assert.equal(sortedModules.length, 2);
    assert.equal(sortedModules[0], 'test-module2');
    assert.equal(sortedModules[1], 'test-module');
  });
});