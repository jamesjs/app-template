const AwaitEventEmitter = require('await-event-emitter');
class JamesAppTemplate extends AwaitEventEmitter {
  constructor() {
    super();

    this.modules = new Map();
    this.moduleOrder = [];
  }

  /**
   * Register multiple modules - if a module is already registered, the existing one with same name will be overwritten
   * @param {Array} modules
   * @returns {Promise<void>}
   */
  async registerModules (modules) {
    for (let module of modules) {
      await this.registerModule(module);
    }
  }

  /**
   * Register a single module - if this module is already registered, the existing one will be overwritten
   * @param {Object} module
   * @returns {Promise<void>}
   */
  async registerModule (module) {
    // store module information - will overwrite existing module definition with same name.
    this.modules.set(module.package.name, module);

    // module initiation
    await module.init(this);

    // inform listeners about the new modue
    this.emit('moduleRegistered', {name: module.package.name, version: module.package.version});
  }

  /**
   * Return module package names ordered by dependencies (first without, last with most complex dependency)
   * @returns {[String]}
   */
  getModulesSortedByDependency () {
    if (this.modules.size !== this.moduleOrder.length) {
      // cached list is outdated
      let registeredModules = Array.from(this.modules.keys());
      this.moduleOrder = [];
      let unresolvedDependencies = new Map();

      // fetch all dependency data per module
      this.modules.forEach((module, name) => {
        let unresolved = [
          ...module.package.dependencies ? Object.keys(module.package.dependencies).filter(d => registeredModules.indexOf(d) !== -1) : [],
          ...module.package.optionalDependencies ? Object.keys(module.package.optionalDependencies).filter(d => registeredModules.indexOf(d) !== -1) : []
        ];
        unresolvedDependencies.set(name, unresolved);
      });

      // try to sort module names by dependency
      while (unresolvedDependencies.size > 0) {
        let changed = false;
        unresolvedDependencies.forEach((deps, module) => {
          deps = deps.filter(d => this.moduleOrder.indexOf(d) === -1);
          if (deps.length === 0) {
            // all dependencies for current module are covered, add module name to list
            this.moduleOrder.push(module);
            changed = true;
            unresolvedDependencies.delete(module);
          } else {
            // update unresolved dependencies - all modules in sorted list are now removed from unresolvedDependencies
            unresolvedDependencies.set(module, deps);
          }
        });
        if (!changed) {
          // no dependency has changed - throw an error.
          throw Error('Endless loop at module dependencies');
        }
      }
    }

    // return cached or new generated list of module names
    return this.moduleOrder;
  }
}

module.exports = JamesAppTemplate;